from src.utils.api_json import get_json

api_url = "https://api.github.com"


def get_code_scanning(repo: dict) -> dict:
    """ This is only available on your own repos
    """
    return get_json(f"{api_url}/repos/{repo['owner']['login']}/{repo['name']}", "code-scanning/alerts", debug=True)
