import re
from json import JSONDecodeError
from typing import List

import requests

from src.headers import init_headers
from src.utils.api_json import get_json

api_url = "https://api.github.com/repos"


def get_repo_from_link(link: str) -> dict:
    """ Returns the Repository object

    :param link: URL in the form of https://github.com/user/repo
    :return: Repository github object from the Github API
    """
    if not link.startswith("https://"):
        raise Exception(f"Link in the wrong format: {link}")

    args: list = link[len("https://"):].split("/")
    if len(args) < 3:
        raise Exception(f"URL was malformed: {link}")

    headers = init_headers()
    response = requests.get(f"{api_url}/{args[1]}/{args[2]}", headers=headers)
    try:
        repository = response.json()
    except JSONDecodeError as e:
        raise Exception(f"Error in parsing the json response.\n{e}")

    return repository


def get_repo_languages(repo: dict) -> List[str]:
    """ Given a repository dictionary, it returns the list of languages used in the repo

    :param repo: repository dictionary (can be obtained using `get_repo_from_link()`)
    :return: list of strings with the languages used in the repository
    """
    parsed: dict = get_json(f"{api_url}/{repo['owner']['login']}/{repo['name']}", "languages")

    return list(parsed.keys())


def get_repo_contributors(repo: dict) -> List[dict]:
    return get_json(f"{api_url}/{repo['owner']['login']}/{repo['name']}", "contributors")


def get_repo_contributors_count(repo: dict) -> int:
    """ Get the number of contributors for a repository
    It's usually not the same as the one on GitHub due to Anonymous contributions

    :param repo: repository dictionary (can be obtained using `get_repo_from_link()`)
    :return:
    """
    headers = init_headers()
    params = {
        "anon": 1,
        "per_page": 1
    }

    path: str = f"{api_url}/{repo['owner']['login']}/{repo['name']}/contributors"
    res = requests.get(path, params=params, headers=headers)

    if 'Link' not in res.headers.keys():
        return -1

    link_header = res.headers['Link']
    links = link_header.split(",")

    if len(links) < 2 or "last" not in links[1]:
        Exception("Error parsing headers")

    contrib_match = re.search(r'[^_]page=([\d]+)', links[1], re.IGNORECASE)

    contributors = -1
    if contrib_match:
        contributors = contrib_match.group(1)

    return int(contributors)
