#!/usr/bin/env python3
import pandas
import pandas as pd
from matplotlib import pyplot as plt
from pandas import DataFrame

from src.dataConverter import hourly_to_df, list_of_users_to_df
from src.repository import *
from src.scanning import get_code_scanning
from src.stats import *

from src.users import get_user_info

from pathlib import Path

# GitHub has a rate limit
repos: list = [
    # get_repo_from_link("https://github.com/torvalds/linux"),
    get_repo_from_link("https://github.com/twbs/bootstrap"),
    get_repo_from_link("https://github.com/nodejs/node"),
    get_repo_from_link("https://github.com/facebook/react"),
    get_repo_from_link("https://github.com/facebook/react-native"),
    get_repo_from_link("https://github.com/tensorflow/tensorflow"),
    get_repo_from_link("https://github.com/git/git"),
    get_repo_from_link("https://github.com/FFmpeg/FFmpeg"),
    get_repo_from_link("https://github.com/dnSpy/dnSpy"),
    get_repo_from_link("https://github.com/electron/electron"),
    get_repo_from_link("https://github.com/godotengine/godot"),
    get_repo_from_link("https://github.com/django/django"),
    get_repo_from_link("https://github.com/neovim/neovim")
]


def weekly_stuff(repo):
    days_index = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
    api_hourly = get_hourly_commit_count(repo)
    hourly_df = hourly_to_df(api_hourly)

    hourly = hourly_df.groupby('Day').sum()
    hourly.index = days_index
    hourly.plot.bar(y='Commits')
    plt.show()

    daily_hours = hourly_df.drop('Day', axis=1).groupby('Hour').sum()
    daily_hours.plot.bar(y='Commits')
    plt.show()


def contributors_loc(repo):
    contributors = get_repo_contributors(repo)
    if type(contributors) is not list:
        return

    df = list_of_users_to_df(contributors)
    df['location'] = [get_user_info(username)['location'] for username in df['login']]
    loc_df = df[['login', 'location']].groupby('location').count()
    print(loc_df)

    contribs = df[['login', 'contributions']]
    contribs.plot.bar(x='login', y='contributions')
    plt.show()


def scanning(repo):
    print(get_code_scanning(repo))


def contribs_df(repo):
    contributors = get_repo_contributors(repo)
    if type(contributors) is not list:
        return None

    return list_of_users_to_df(contributors)


def create_contribs_db(repositories):
    df = DataFrame()
    for repo in repositories:
        tmp = contribs_df(repo)
        if tmp is None:
            continue
        tmp['repo'] = repo['name']
        df = pandas.concat([df, tmp])
    Path("dbs").mkdir(parents=True, exist_ok=True)
    df.to_csv("dbs/contribs.csv")


def create_commits_db(repositories):
    df1 = DataFrame()
    df2 = DataFrame()
    for repo in repositories:
        days_index = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
        api_hourly = get_hourly_commit_count(repo)
        hourly_df = hourly_to_df(api_hourly)

        hourly = hourly_df.groupby('Day').sum()
        hourly.index = days_index
        hourly['repo'] = repo['name']
        df1 = pandas.concat([df1, hourly])

        daily_hours = hourly_df.drop('Day', axis=1).groupby('Hour').sum()
        daily_hours['repo'] = repo['name']
        df2 = pandas.concat([df2, daily_hours])

    Path("dbs").mkdir(parents=True, exist_ok=True)
    df1.to_csv("dbs/hourly.csv")
    df2.to_csv("dbs/daily.csv")


def users_with_location(repositories):
    tmp = pd.DataFrame()
    for repo in repositories:
        contributors = get_repo_contributors(repo)
        if type(contributors) is not list:
            return

        df = list_of_users_to_df(contributors)
        df['location'] = [get_user_info(username)['location'] for username in df['login']]
        tmp = pd.concat([tmp, df])

    Path("dbs").mkdir(parents=True, exist_ok=True)
    tmp.to_csv("dbs/locations.csv")


def languages_db(repositories):
    reps = DataFrame()
    for rep in repositories:
        aux = get_repo_languages(rep)
        df = DataFrame()
        df['languages'] = aux
        df['repo'] = rep['name']
        reps = pd.concat([reps, df])
    Path("dbs").mkdir(parents=True, exist_ok=True)
    reps.to_csv("dbs/repos_lang.csv")


if __name__ == "__main__":
    # weekly_stuff(repos[0])
    # contributors_loc(repos[0])
    # scanning(repos[0])
    # create_contribs_db(repos)
    # create_commits_db(repos)
    # users_with_location(repos)
    languages_db(repos)
