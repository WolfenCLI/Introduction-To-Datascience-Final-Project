from typing import List
from src.utils.api_json import get_json

api_url = "https://api.github.com/repos"


def get_weekly_commit_activity(repo: dict) -> List[list]:
    """ Returns a list of lists, every element of the outer list is going to contain:
    - unix timestamp
    - rows added
    - rows deleted

    :param repo: repository dictionary (can be obtained using `get_repo_from_link()`)
    :return:
    """
    return get_json(f"{api_url}/{repo['owner']['login']}/{repo['name']}/stats", "code_frequency")


def get_monthly_commit_activity(repo: dict) -> List[list]:
    """ Returns a list of dicts, every element being a week
    - unix timestamp
    - rows added
    - rows deleted

    :param repo: repository dictionary (can be obtained using `get_repo_from_link()`)
    :return:
    """
    return get_json(f"{api_url}/{repo['owner']['login']}/{repo['name']}/stats", "commit_activity")


def get_contributors_activity(repo: dict) -> dict:
    """ Returns a list with each element being a dictionary with:
    - total: int
    - weeks: List{dict}
    - author: dict

    each entry of `weeks`: 'w' start of the week, 'a' Number of additions,'d' Number of deletions,'c' Number of commits

    :param repo: repository dictionary (can be obtained using `get_repo_from_link()`)
    :return:
    """
    return get_json(f"{api_url}/{repo['owner']['login']}/{repo['name']}/stats", "contributors")


def get_hourly_commit_count(repo: dict) -> dict:
    """ Each array contains the day number, hour number, and number of commits

    :param repo: `dict` repository dictionary (can be obtained using `get_repo_from_link()`)
    :return: `List[list]`
    """
    return get_json(f"{api_url}/{repo['owner']['login']}/{repo['name']}/stats", "punch_card")
