from typing import List

import pandas as pd


def hourly_to_df(data: dict):
    return pd.DataFrame(data, columns=["Day", "Hour", "Commits"])


def list_of_users_to_df(users: List[dict]):
    return pd.DataFrame(users)
