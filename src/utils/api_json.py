from json import JSONDecodeError
from typing import List, Dict

import requests

from src.headers import init_headers


def get_json(base_url: str, endpoint: str = "", headers=None, params=None, debug=False) -> List or Dict:
    if headers is None:
        headers = init_headers()
    if params is None:
        params = {}
    path: str = f"{base_url}/{endpoint}"

    res = requests.get(path, headers=headers, params=params)

    if debug:
        print(f"Status code: {res.status_code}")

    try:
        parsed = res.json()
    except JSONDecodeError as e:
        raise Exception(f"Error in parsing the json response.\n{e}")

    return parsed
