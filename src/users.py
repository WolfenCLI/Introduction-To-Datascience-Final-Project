from src.utils.api_json import get_json

api_url = "https://api.github.com/users"


def get_user_info(username: str) -> dict:
    return get_json(api_url, username)
