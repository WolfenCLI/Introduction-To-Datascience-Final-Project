import os
from base64 import b64encode

token = os.environ['TOKEN']
user = os.environ['USER']


def init_headers() -> dict:
    res = {
        "accept": "application/vnd.github.v3+json"
    }
    if token is not None and user is not None:
        auth: str = b64encode(f"{user}:{token}".encode()).decode("ascii")
        res['Authorization'] = f"Basic {auth}"

    return res
