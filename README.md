# Introduction-To-Datascience-Final-Project
Final Project for my Introduction to Data Science class

## Environment Variables
The GitHub API has different rate limits for authenticated users, if you want to use the
authenticated version of the API calls, please set the following environment variables:
- `TOKEN` Your [GitHub Token](https://github.com/settings/tokens)
- `USER` Your GitHub username